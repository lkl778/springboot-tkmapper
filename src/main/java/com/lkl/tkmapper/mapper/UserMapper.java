package com.lkl.tkmapper.mapper;

import com.lkl.tkmapper.base.BaseMapper;
import com.lkl.tkmapper.entity.User;

public interface UserMapper extends BaseMapper<User> {
}