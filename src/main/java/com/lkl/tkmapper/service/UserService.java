package com.lkl.tkmapper.service;

import com.lkl.tkmapper.entity.User;

import java.util.List;

/**
 * @author likelong
 * @date 2022/8/6
 */
public interface UserService {

    User getById(int id);

    List<User> list();
}
