package com.lkl.tkmapper.service.impl;

import com.lkl.tkmapper.entity.User;
import com.lkl.tkmapper.mapper.UserMapper;
import com.lkl.tkmapper.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author likelong
 * @date 2022/8/6
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public User getById(int id) {
        return userMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<User> list() {
        return userMapper.selectAll();
    }
}
