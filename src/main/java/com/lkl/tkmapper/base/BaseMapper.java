package com.lkl.tkmapper.base;

import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

/**
 * tkmapper的公共接口 供其它接口继承
 *
 * @author likelong
 * @date 2022/8/5
 */
public interface BaseMapper<T> extends Mapper<T>, MySqlMapper<T> {
}
