package com.lkl.tkmapper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import tk.mybatis.spring.annotation.MapperScan;

@MapperScan("com.lkl.tkmapper.mapper")
@SpringBootApplication
public class SpringbootTkmapperApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootTkmapperApplication.class, args);
    }

}
