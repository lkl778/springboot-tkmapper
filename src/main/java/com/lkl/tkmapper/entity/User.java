package com.lkl.tkmapper.entity;

import lombok.Data;

import javax.persistence.*;


@Data
public class User {

    @Id
    private Integer id;

    private String userName;

    private String password;

    private String address;

    private String phone;

}
