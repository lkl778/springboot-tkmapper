package com.lkl.tkmapper;

import com.lkl.tkmapper.entity.User;
import com.lkl.tkmapper.mapper.UserMapper;
import com.lkl.tkmapper.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

@SpringBootTest
class SpringbootTkmapperApplicationTests {

    @Autowired
    private UserService userService;

    @Autowired
    private UserMapper userMapper;

    @Test
    void contextLoads() {

        User user = userService.getById(1);
        System.out.println(user);

        System.out.println(userService.list());

        Example example = new Example(User.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("userName", "likelong");
        List<User> users = userMapper.selectByExample(example);
        System.out.println("-------------");
        System.out.println(users);
    }

}
